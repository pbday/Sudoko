#lang racket/base

(require rackunit
         "solver.rkt") 

;Test overview
;
;These tests show basic coverage of the functions used in the program.
; They do not test that the input is valid, i.e. 0 - 9 integers, nor contradictory,
; nor show full coverage as it was said in the lecture that tests should only be indicative
; and it would tkae a lot of work which was not the goal of the exercise.

;Key methods of functionality have been tested here (rather than all smaller sub methods):
  ;-Solve as defined within spec
  ;-Transform as defined in the spec
  ;-GetNextDetails - Finds the next singleton and returns the number and location of it
  ;-WorkRows - Given a location to work, removes all dependencies
  ;-FindNewSingletons - Looks for necessary singletons due to holding a uniquer value in a row, column or box
  ;-SetPosTo - Changes a value in a matrix

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Data
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define testExampleInput (list
                    '(0 2 5 0 0 1 0 0 0)
                    '(1 0 4 2 5 0 0 0 0)
                    '(0 0 6 0 0 4 2 1 0)
                    '(0 5 0 0 0 0 3 2 0)
                    '(6 0 0 0 2 0 0 0 9)
                    '(0 8 7 0 0 0 0 6 0)
                    '(0 9 1 5 0 0 6 0 0)
                    '(0 0 0 0 7 8 1 0 3)
                    '(0 0 0 6 0 0 5 9 0)))

(define testExampleOutput ( list
  (list '(8) '(2) '(5) '(7) '(6) '(1) '(9) '(3) '(4))
  (list '(1) '(3) '(4) '(2) '(5) '(9) '(7) '(8) '(6))
  (list '(9) '(7) '(6) '(8) '(3) '(4) '(2) '(1) '(5))
  (list '(4) '(5) '(9) '(1) '(8) '(6) '(3) '(2) '(7))
  (list '(6) '(1) '(3) '(4) '(2) '(7) '(8) '(5) '(9))
  (list '(2) '(8) '(7) '(3) '(9) '(5) '(4) '(6) '(1))
  (list '(3) '(9) '(1) '(5) '(4) '(2) '(6) '(7) '(8))
  (list '(5) '(6) '(2) '(9) '(7) '(8) '(1) '(4) '(3))
  (list '(7) '(4) '(8) '(6) '(1) '(3) '(5) '(9) '(2))))

(define testNoSolutionInput (list
                    '(0 2 3 4 5 6 7 8 0)
                    '(4 5 6 7 8 0 0 2 3)
                    '(7 8 0 0 2 3 4 5 6)
                    '(2 3 0 5 6 4 8 0 7)
                    '(5 6 4 8 0 7 2 3 0)
                    '(8 0 7 2 3 0 5 6 4)
                    '(3 0 2 6 4 5 0 7 8)
                    '(6 4 5 0 7 8 3 0 2)
                    '(0 7 8 3 0 2 6 4 5)))

(define testNoSolutionOutput
  '(((1 9) (2) (3) (4) (5) (6) (7) (8) (1 9))
    ((4) (5) (6) (7) (8) (1 9) (1 9) (2) (3))
    ((7) (8) (1 9) (1 9) (2) (3) (4) (5) (6))
    ((2) (3) (1 9) (5) (6) (4) (8) (1 9) (7))
    ((5) (6) (4) (8) (1 9) (7) (2) (3) (1 9))
    ((8) (1 9) (7) (2) (3) (1 9) (5) (6) (4))
    ((3) (1 9) (2) (6) (4) (5) (1 9) (7) (8))
    ((6) (4) (5) (1 9) (7) (8) (3) (1 9) (2))
   ((1 9) (7) (8) (3) (1 9) (2) (6) (4) (5))))


(define testInput2 (list
                    '(1 2 3 4 5 6 7 8 9)
                    '(4 5 6 7 8 9 1 2 3)
                    '(7 8 9 1 2 3 4 5 6)
                    '(2 3 1 5 6 4 8 9 7)
                    '(5 6 4 8 9 7 2 3 1)
                    '(8 9 7 2 3 1 5 6 4)
                    '(3 1 2 6 4 5 9 7 8)
                    '(6 4 5 9 7 8 3 1 2)
                    '(0 0 0 0 0 0 0 0 0)))

(define tranMatrix2
  '(((1) (2) (3) (4) (5) (6) (7) (8) (9))
    ((4) (5) (6) (7) (8) (9) (1) (2) (3))
    ((7) (8) (9) (1) (2) (3) (4) (5) (6))
    ((2) (3) (1) (5) (6) (4) (8) (9) (7))
    ((5) (6) (4) (8) (9) (7) (2) (3) (1))
    ((8) (9) (7) (2) (3) (1) (5) (6) (4))
    ((3) (1) (2) (6) (4) (5) (9) (7) (8))
    ((6) (4) (5) (9) (7) (8) (3) (1) (2))
   ((1 2 3 4 5 6 7 8 9) (1 2 3 4 5 6 7 8 9) (1 2 3 4 5 6 7 8 9) (1 2 3 4 5 6 7 8 9) (1 2 3 4 5 6 7 8 9) (1 2 3 4 5 6 7 8 9) (1 2 3 4 5 6 7 8 9) (1 2 3 4 5 6 7 8 9) (1 2 3 4 5 6 7 8 9))))

(define testTranBefore (list '(1 2 3) '(4 5 6) '(7 8 9)))
(define testTranAfter (list (list '(1) '(2) '(3)) (list '(4) '(5) '(6)) (list '(7) '(8) '(9))))

(define checkFalse '((#t #t #t #t #t #t #t #t #t)
  (#t #t #t #t #t #t #t #t #t)
  (#t #t #t #t #t #t #t #t #t)
  (#t #t #t #t #t #t #t #t #t)
  (#t #t #t #t #t #t #t #t #t)
  (#t #t #t #t #t #t #t #t #t)
  (#t #t #t #t #t #t #t #t #t)
  (#t #t #t #t #t #t #t #t #t)
  (#f #f #f #f #f #f #f #f #f)))

(define testSingToFind
  '(((1) (2) (3) (4) (5) (6) (7) (8) (9))
    ((4) (5) (6) (7) (8) (9) (1) (2) (3))
    ((7) (8) (9) (1) (2) (3) (4) (5) (6))
    ((2) (3) (1) (5) (6) (4) (8) (9) (7))
    ((5) (6) (4) (8) (9) (7) (2) (3) (1))
    ((8) (9) (7) (2) (3) (1) (5) (6) (4))
    ((3) (1) (2) (6) (4) (5) (9) (7) (8))
    ((6) (4) (5) (9) (7) (8) (3) (1) (2))
   ((9) (1 2 3 4 5 6 7 8 9) (1 2 3 4 5 6 7 8 9) (1 2 3 4 5 6 7 8 9) (1 2 3 4 5 6 7 8 9) (1 2 3 4 5 6 7 8 9) (1 2 3 4 5 6 7 8 9) (1 2 3 4 5 6 7 8 9) (1 2 3 4 5 6 7 8 9))))

(define testRemoveSingRowBefore
    '(((1) (2) (3) (4) (5) (6) (7) (8) (9))
    ((4) (5) (6) (7) (8) (9) (1) (2) (3))
    ((7) (8) (9) (1) (2) (3) (4) (5) (6))
    ((2) (3) (1) (5) (6) (4) (8) (9) (7))
    ((5) (6) (4) (8) (9) (7) (2) (3) (1))
    ((8) (9) (7) (2) (3) (1) (5) (6) (4))
    ((3) (1) (2) (6) (4) (5) (9) (7) (8))
    ((6) (4) (5) (9) (7) (8) (3) (1) (2))
   ((9) (2 9) (3) (4) (5) (6) (7) (8) (8))))

(define testRemoveSingColBefore
    '(((1) (2) (3) (4) (5) (6) (7) (8) (9))
    ((4) (5) (6) (7) (8) (9) (1) (2) (3))
    ((7) (8) (9) (1) (2) (3) (4) (5) (6))
    ((2) (3) (1) (5) (6) (4) (8) (9) (7))
    ((5) (6) (4) (8) (9) (7) (2) (3) (1))
    ((8) (9) (7) (2) (3) (1) (5) (6) (4))
    ((3) (1) (2) (6) (4) (5) (9) (7) (8))
    ((6 9) (4) (5) (9) (7) (8) (3) (1) (2))
   ((9) (2) (3) (4) (5) (6) (7) (8) (8))))

(define testRemoveSingBoxBefore
    '(((1) (2) (3) (4) (5) (6) (7) (8) (9))
    ((4) (5) (6) (7) (8) (9) (1) (2) (3))
    ((7) (8) (9) (1) (2) (3) (4) (5) (6))
    ((2) (3) (1) (5) (6) (4) (8) (9) (7))
    ((5) (6) (4) (8) (9) (7) (2) (3) (1))
    ((8) (9) (7) (2) (3) (1) (5) (6) (4))
    ((3) (1) (2 9) (6) (4) (5) (9) (7) (8))
    ((6) (4) (5) (9) (7) (8) (3) (1) (2))
   ((9) (2) (3) (4) (5) (6) (7) (8) (8))))

(define testRemoveSingAfter
    '(((1) (2) (3) (4) (5) (6) (7) (8) (9))
    ((4) (5) (6) (7) (8) (9) (1) (2) (3))
    ((7) (8) (9) (1) (2) (3) (4) (5) (6))
    ((2) (3) (1) (5) (6) (4) (8) (9) (7))
    ((5) (6) (4) (8) (9) (7) (2) (3) (1))
    ((8) (9) (7) (2) (3) (1) (5) (6) (4))
    ((3) (1) (2) (6) (4) (5) (9) (7) (8))
    ((6) (4) (5) (9) (7) (8) (3) (1) (2))
   ((9) (2) (3) (4) (5) (6) (7) (8) (8))))

(define testSingToFindRowBecauseUnique
  '(((1 9) (2) (3) (4) (5) (6) (7) (8) (1))
    ((4) (5) (6) (7) (8) (1 9) (1 9) (2) (3))
    ((7) (8) (1 9) (1 9) (2) (3) (4) (5) (6))
    ((2) (3) (1 9) (5) (6) (4) (8) (1 9) (7))
    ((5) (6) (4) (8) (1 9) (7) (2) (3) (1 9))
    ((8) (1 9) (7) (2) (3) (1 9) (5) (6) (4))
    ((3) (1 9) (2) (6) (4) (5) (1 9) (7) (8))
    ((6) (4) (5) (1 9) (7) (8) (3) (1 9) (2))
   ((1 9) (7) (8) (3) (1 9) (2) (6) (4) (5))))

(define testSingToFindRowSoln
  '(((9) (2) (3) (4) (5) (6) (7) (8) (1))
    ((4) (5) (6) (7) (8) (1 9) (1 9) (2) (3))
    ((7) (8) (1 9) (1 9) (2) (3) (4) (5) (6))
    ((2) (3) (1 9) (5) (6) (4) (8) (1 9) (7))
    ((5) (6) (4) (8) (1 9) (7) (2) (3) (1 9))
    ((8) (1 9) (7) (2) (3) (1 9) (5) (6) (4))
    ((3) (1 9) (2) (6) (4) (5) (1 9) (7) (8))
    ((6) (4) (5) (1 9) (7) (8) (3) (1 9) (2))
   ((1 9) (7) (8) (3) (1 9) (2) (6) (4) (5))))

(define testSingToFindColBecauseUnique
  '(((1 9) (2) (3) (4) (5) (6) (7) (8) (1 9))
    ((4) (5) (6) (7) (8) (1 9) (1 9) (2) (3))
    ((7) (8) (1 9) (1 9) (2) (3) (4) (5) (6))
    ((2) (3) (1 9) (5) (6) (4) (8) (1 9) (7))
    ((5) (6) (4) (8) (1 9) (7) (2) (3) (1 9))
    ((8) (1 9) (7) (2) (3) (1 9) (5) (6) (4))
    ((3) (1 9) (2) (6) (4) (5) (1 9) (7) (8))
    ((6) (4) (5) (1 9) (7) (8) (3) (1 9) (2))
   ((1) (7) (8) (3) (1 2 9) (2 9) (6) (4) (5))))

(define testSingToFindColSoln
  '(((9) (2) (3) (4) (5) (6) (7) (8) (1 9))
    ((4) (5) (6) (7) (8) (1 9) (1 9) (2) (3))
    ((7) (8) (1 9) (1 9) (2) (3) (4) (5) (6))
    ((2) (3) (1 9) (5) (6) (4) (8) (1 9) (7))
    ((5) (6) (4) (8) (1 9) (7) (2) (3) (1 9))
    ((8) (1 9) (7) (2) (3) (1 9) (5) (6) (4))
    ((3) (1 9) (2) (6) (4) (5) (1 9) (7) (8))
    ((6) (4) (5) (1 9) (7) (8) (3) (1 9) (2))
   ((1) (7) (8) (3) (1 2 9) (2 9) (6) (4) (5))))

(define testSingToFindBoxBecauseUnique
  '(((1 9) (2) (3) (4) (5) (6) (7) (8) (1 9))
    ((4) (5) (6) (7) (8) (1 9) (1 9) (2) (3))
    ((7) (8) (1) (1 9) (1 9) (3) (4) (5) (6))
    ((2) (3) (1 9) (5) (6) (4) (8) (1 9) (7))
    ((5) (6) (4) (8) (1 9) (7) (2) (3) (1 9))
    ((8) (1 9) (7) (2) (3) (1 9) (5) (6) (4))
    ((3) (1 9) (2) (6) (4) (5) (1 9) (7) (8))
    ((6) (4) (1 9) (1 9) (7) (8) (3) (1 9) (2))
   ((1 9) (7) (8) (3) (1 9) (2) (6) (4) (5))))

(define testSingToFindBoxSoln
  '(((9) (2) (3) (4) (5) (6) (7) (8) (1 9))
    ((4) (5) (6) (7) (8) (1 9) (1 9) (2) (3))
    ((7) (8) (1) (1 9) (1 9) (3) (4) (5) (6))
    ((2) (3) (1 9) (5) (6) (4) (8) (1 9) (7))
    ((5) (6) (4) (8) (1 9) (7) (2) (3) (1 9))
    ((8) (1 9) (7) (2) (3) (1 9) (5) (6) (4))
    ((3) (1 9) (2) (6) (4) (5) (1 9) (7) (8))
    ((6) (4) (1 9) (1 9) (7) (8) (3) (1 9) (2))
   ((1 9) (7) (8) (3) (1 9) (2) (6) (4) (5))))

(define testChangePosTFBefore '((#f #f #f #f #f #f #f #f #f)
  (#f #f #f #f #f #f #f #f #f)
  (#f #f #f #f #f #f #f #f #f)
  (#f #f #f #f #f #f #f #f #f)
  (#f #f #f #f #f #f #f #f #f)
  (#f #f #f #f #f #f #f #f #f)
  (#f #f #f #f #f #f #f #f #f)
  (#f #f #f #f #f #f #f #f #f)
  (#f #f #f #f #f #f #f #f #f)))

(define testChangePosTFAfter '((#f #f #f #f #f #f #f #f #f)
  (#f #f #t #f #f #f #f #f #f)
  (#f #f #f #f #f #f #f #f #f)
  (#f #f #f #f #f #f #f #f #f)
  (#f #f #f #f #f #f #f #f #f)
  (#f #f #f #f #f #f #f #f #f)
  (#f #f #f #f #f #f #f #f #f)
  (#f #f #f #f #f #f #f #f #f)
  (#f #f #f #f #f #f #f #f #f)))

(define testChangePosNumber ( list
  (list '(8) '(2) '(5) '(7) '(6) '(1) '(9) '(3) '(4))
  (list '(1) '(1) '(4) '(2) '(5) '(9) '(7) '(8) '(6))
  (list '(9) '(7) '(6) '(8) '(3) '(4) '(2) '(1) '(5))
  (list '(4) '(5) '(9) '(1) '(8) '(6) '(3) '(2) '(7))
  (list '(6) '(1) '(3) '(4) '(2) '(7) '(8) '(5) '(9))
  (list '(2) '(8) '(7) '(3) '(9) '(5) '(4) '(6) '(1))
  (list '(3) '(9) '(1) '(5) '(4) '(2) '(6) '(7) '(8))
  (list '(5) '(6) '(2) '(9) '(7) '(8) '(1) '(4) '(3))
  (list '(7) '(4) '(8) '(6) '(1) '(3) '(5) '(9) '(2))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Tests
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Solve
 ;Input matrix example solution
(check-equal? (solve testExampleInput) testExampleOutput "Example solvable solution not solved")
 ;Test no full solution usiong outlined method possible
(check-equal? (solve testNoSolutionInput) testNoSolutionOutput "Unsolvable solution not returned properly")

;transform
 ; row with zero's
(check-equal? (transform testTranBefore) testTranAfter "Transform error turning integers into lists lists")
 ; row with zero's
(check-equal? (transform testInput2) tranMatrix2 "Transform error transforming zeros")

;GetNextDetails
 ; giving nothing
(check-equal? (getNextDetails tranMatrix2 checkFalse) '(#f) "Error: GetNextDetails not return false in a list where no singletons")
 ; with a singleton to find
(check-equal? (getNextDetails testSingToFind checkFalse) '(#t 9 8 0) "Error: GetNextDetails not find singleton")

;workRows
 ;check row removed
(check-equal? (workRows testRemoveSingRowBefore 9 8 0 0 '()) testRemoveSingAfter "Error: Numbers on the row not correctly removed")
 ;check column removed
(check-equal? (workRows testRemoveSingColBefore 9 8 0 0 '()) testRemoveSingAfter "Error: Numbers on the column not correctly removed")
 ; check box removed
(check-equal? (workRows testRemoveSingBoxBefore 9 8 0 0 '()) testRemoveSingAfter "Error: Numbers on the box not correctly removed")

;Find new singletons
 ;give grid with row dependency to find
(check-equal? (car (findNewSingletons testSingToFindRowBecauseUnique)) #t "Error: True not returned for finding singletons as unique value in row")
(check-equal? (cadr (findNewSingletons testSingToFindRowBecauseUnique)) testSingToFindRowSoln "Error: Doesn't correctly find singletons as unique value in row")
 ; col dependency to find
(check-equal? (car (findNewSingletons testSingToFindColBecauseUnique)) #t "Error: True not returned for finding singletons as unique value in column")
(check-equal? (cadr (findNewSingletons testSingToFindColBecauseUnique)) testSingToFindColSoln "Error: Doesn't correctly find singletons as unique value in column")
 ; box dependency to find
(check-equal? (car (findNewSingletons testSingToFindBoxBecauseUnique)) #t "Error: True not returned for finding singletons as unique value in box")
(check-equal? (cadr (findNewSingletons testSingToFindBoxBecauseUnique)) testSingToFindBoxSoln "Error: Doesn't correctly find singletons as unique value in box")

;setPosTo
 ; change a true/false
(check-equal? (setPosTo testChangePosTFBefore #t 1 2) testChangePosTFAfter "Error: Set position in matrix not work for true/false")
 ; insert a list
(check-equal? (setPosTo testExampleOutput '(1) 1 1) testChangePosNumber "Error: Set position in matrix not work for numbers")

(write "Tests completed.")